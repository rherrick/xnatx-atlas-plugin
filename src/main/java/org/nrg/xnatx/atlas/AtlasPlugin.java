package org.nrg.xnatx.atlas;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.om.XnatxAtlassessiondata;

@XnatPlugin(value = "xnatxAtlasPlugin", name = "XNATX Brain Atlas Plugin",
            dataModels = {@XnatDataModel(value = XnatxAtlassessiondata.SCHEMA_ELEMENT_NAME,
                                         code = "ATL",
                                         singular = "Brain Atlas",
                                         plural = "Brain Atlases")})
@Slf4j
public class AtlasPlugin {
    public AtlasPlugin() {
        log.debug("Loading the AtlasPlugin configuration");
    }
}
